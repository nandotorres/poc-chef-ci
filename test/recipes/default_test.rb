describe command('service docker status') do
  its(:stdout) { should match /Docker is running/m }
end